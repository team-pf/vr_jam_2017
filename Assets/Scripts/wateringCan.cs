﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wateringCan : MonoBehaviour {

	Vector3 oldEulerAngles;
	public GameObject can;
	public ParticleSystem water;
	public bool isWatering;
	public GameObject particles;

	// Use this for initialization
	void Start () {
		oldEulerAngles = can.transform.rotation.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {

		/*if (can.transform.parent.localRotation.x > -0.22f) {
			water.Play();
			Debug.Log ("Water on");
		}
		else {
			//No Rotation
			water.Stop ();
			Debug.Log ("Water stopped.");
		}*/

		//if (oldEulerAngles == can.transform.rotation.eulerAngles) {
//		Debug.Log(Vector3.Dot (Vector3.up, water.transform.forward) );
		if (Vector3.Dot (Vector3.up, water.transform.forward) < 0) {
			if (!water.isPlaying) {
				water.Play ();
				particles.SetActive (true);
			}

		} else {
			if (water.isPlaying) {
				water.Stop ();
				particles.SetActive (false);
			}
		}
		/*
		if (Mathf.Abs (oldEulerAngles.x - can.transform.parent.rotation.eulerAngles.x) < 0.1f) {
			//No Rotation
			water.Stop ();
			Debug.Log ("Water stopped.");

		}
		else {
			oldEulerAngles = can.transform.rotation.eulerAngles;
			water.Play();
			Debug.Log ("Water on");
		}*/
	}
}
