﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketScript : MonoBehaviour {

    public TextMesh dollarValueTextMesh;
    public AudioClip cashRegisterClip;

    AudioSource sound;

    int dollarValue;

	// Use this for initialization
	void Start () {
        dollarValue = 0;
        sound = GetComponent<AudioSource>();
	}
	
    void OnSold()
    {
        dollarValue += 1;
        dollarValueTextMesh.text = "$ " + dollarValue + ".00";
        sound.PlayOneShot(cashRegisterClip);
    }

	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == "turnip holder" ||
			collider.gameObject.name == "Cabbage" ||
			collider.gameObject.name == "Carrot")
        {
            Debug.Log("Vegetable sold");
            OnSold();
            Destroy(collider.gameObject);
        }
    }

}
