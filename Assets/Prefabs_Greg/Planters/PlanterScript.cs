﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanterScript : MonoBehaviour {

    enum PlanterState { waitingSeed, waitingWater, waitingGrowth, waitingHarvest };
    enum PlantType { carrot };
    const float switchPeriod = 5.0f;

    Transform    carrot;
    Transform    turnip;
	Transform    cabbage;
    Transform    wateringSign;
    Transform    doneSign;
    Transform    earth;
    Transform    growingPlant;
    float        timeUntilSwitch;
    PlanterState planterState;
    PlantType    plantType;

    private AudioSource source;

    public Material planterEarthDry;
    public Material planterEarthWet;
    public AudioClip doneSound;
    public AudioClip growingSound;

    // Use this for initialization
    void Start () {
        planterState = PlanterState.waitingSeed;
        timeUntilSwitch = switchPeriod;
        foreach (Transform t in transform)
        {
            if (t.name == "Plants")
            {
                foreach (Transform plants in t)
                {
                    if (plants.name == "Carrot")
                    {
                        carrot = plants;
                    }
                    else if (plants.name == "turnip holder")
                    {
                        turnip = plants;
                    }
					else if (plants.name == "Cabbage")
					{
						cabbage = plants;
					}
                }
            }
            else if (t.name == "Icons")
            {
                foreach (Transform icons in t)
                {
                    if (icons.name == "Water")
                    {
                        wateringSign = icons;
                    }
                    else if (icons.name == "Done")
                    {
                        doneSign = icons;
                    }
                }
            }
            else if (t.name == "Earth")
            {
                earth = t;
            }
        }
        source = GetComponent<AudioSource>();
    }
	
	public void OnSeed(string seedName)
    {
        planterState = PlanterState.waitingWater;
        wateringSign.gameObject.SetActive(true);
		if (seedName == "seed") {
			growingPlant = turnip;		
		} else if (seedName == "seed2") {
			growingPlant = carrot;		
		} else if (seedName == "Kidney_Bean") {
			growingPlant = cabbage;		
		} else {
			Debug.LogError ("Unknown seed" + seedName);
		}
    }

    void OnWater()
    {
        planterState = PlanterState.waitingGrowth;
        wateringSign.gameObject.SetActive(false);

        Material[] mats = earth.gameObject.GetComponent<Renderer>().materials;
        mats[0] = planterEarthWet;
        earth.gameObject.GetComponent<Renderer>().materials = mats;

        growingPlant.gameObject.SetActive(true);

        source.loop = true;
        source.clip = growingSound;
        source.Play();
    }

    void OnGrowth()
    {
        planterState = PlanterState.waitingHarvest;
        doneSign.gameObject.SetActive(true);

        Material[] mats = earth.GetComponent<Renderer>().materials;
        mats[0] = planterEarthDry;
        earth.GetComponent<Renderer>().materials = mats;

        growingPlant.localScale = Vector3.one;

        source.Stop();
        source.PlayOneShot(doneSound);
    }

    void OnHarvest()
    {
        planterState = PlanterState.waitingSeed;
        doneSign.gameObject.SetActive(false);
        growingPlant.gameObject.SetActive(false);
    }

    void OnSwitch()
    {
        switch(planterState)
        {
            case PlanterState.waitingSeed:
//                OnSeed();
                return;
            case PlanterState.waitingWater:
                OnWater();
                return;
            case PlanterState.waitingGrowth:
                OnGrowth();
                return;
            case PlanterState.waitingHarvest:
                OnHarvest();
                return;
            default:
                Debug.LogError("Unknown planter state !");
                break;
        }
    }

    void UpdateGrowth()
    {
        if (planterState == PlanterState.waitingGrowth)
        {
			float deltaTime = Time.deltaTime;
			if (timeUntilSwitch < deltaTime)
			{
				OnSwitch();
				timeUntilSwitch = switchPeriod;
			}
			else
			{
				timeUntilSwitch -= deltaTime;
				float scale = (switchPeriod - timeUntilSwitch) / switchPeriod;
				growingPlant.localScale = Vector3.one * scale;
			}
        }
    }

    // Update is called once per frame
    void Update () {
        UpdateGrowth();
    }

    public void Execute()
    {

    }

	void OnTriggerEnter(Collider collider) {
//		seed.GetComponent<PlanterScript> ().OnSeed();
		if (planterState == PlanterState.waitingSeed &&
			collider.gameObject.name == "seed" ||
			collider.gameObject.name == "seed2" ||
			collider.gameObject.name == "Kidney_Bean") {
			OnSeed (collider.gameObject.name);
			Destroy (collider.gameObject);
		} else if (planterState == PlanterState.waitingWater &&
			(collider.gameObject.tag == "Water" ||
				collider.gameObject.name == "collider")) {
			Debug.Log ("Water hit");
			OnWater ();
		}
	}

	void OnParticleCollision (GameObject otherObject)
	{
		Debug.Log ("OnParticleCollision hit");
		if (otherObject.tag == "Water") {
			OnWater ();
		}
	}
}
